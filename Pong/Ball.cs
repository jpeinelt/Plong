﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong
{
    class Ball
    {
        #region Fields

        // govement
        Vector2 velocity = new Vector2(0, 0);

        // graphic and drawing
        Texture2D sprite;
        Rectangle drawRectangle;
        int halfHeight;

        #endregion

        #region Constructors

        public Ball(ContentManager contentManager, string spriteName, int x, int y)
        {
            LoadContent(contentManager, spriteName, x, y);
            halfHeight = sprite.Height / 2;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the collision rectangle for the ball
        /// </summary>
        public Rectangle CollisionRectangle
        {
            get { return drawRectangle; }
        }

        /// <summary>
        /// Gets and sets the velocity of the ball
        /// </summary>
        public Vector2 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        /// <summary>
        /// Gets and sets the mid point of the ball
        /// </summary>
        public Point Middle
        {
            get { return new Point(drawRectangle.Location.X - halfHeight, drawRectangle.Location.Y - halfHeight); }
            set { drawRectangle.X = value.X - sprite.Width / 2; drawRectangle.Y = value.Y - halfHeight; }
        }


        #endregion

        #region Public methods

        /// <summary>
        /// Updateds the location of the ball, including bouncing
        /// </summary>
        /// <param name="gameTime">game time</param>
        public void Update(GameTime gameTime)
        {
            var milliseconds = gameTime.ElapsedGameTime.Milliseconds;
            drawRectangle.X += (int)(velocity.X * milliseconds);
            drawRectangle.Y += (int)(velocity.Y * milliseconds);
            BounceTopBottom();
        }

        /// <summary>
        /// draws the ball
        /// </summary>
        /// <param name="spriteBatch">sprite batch to use</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, drawRectangle, Color.White);
        }

        #endregion

        #region Private methods

        private void LoadContent(ContentManager contentManager, string spriteName, int x, int y)
        {
            sprite = contentManager.Load<Texture2D>(spriteName);
            drawRectangle = new Rectangle(x - sprite.Width / 2,
                y - sprite.Height / 2, sprite.Width,
                sprite.Height);
        }

        /// <summary>
        /// Bounces the ball off the top and bottom window borders if necessary
        /// </summary>
        private void BounceTopBottom()
        {
            if (drawRectangle.Y < 0)
            {
                // bounce off top
                drawRectangle.Y = 0;
                velocity.Y *= -1;
            }
            else if ((drawRectangle.Y + drawRectangle.Height) > GameConstants.WindowHeight)
            {
                // bounce off bottom
                drawRectangle.Y = GameConstants.WindowHeight - drawRectangle.Height;
                velocity.Y *= -1;
            }
        }

        #endregion
    }
}
