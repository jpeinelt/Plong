﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using System;

namespace Pong
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Paddle playerPaddle;
        Paddle enemyPaddle;
        Ball ball;

        int[] score = new int[2] { 0, 0 };
        string scoreString = "0 - 0";

        SpriteFont font;

        SoundEffect playerWin;
        SoundEffect playerLose;
        SoundEffect paddleStrike;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // set resolution
            graphics.PreferredBackBufferWidth = GameConstants.WindowWidth;
            graphics.PreferredBackBufferHeight = GameConstants.WindowHeight;

            RandomNumberGenerator.Initialize();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            RandomNumberGenerator.Initialize();
            
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: load font and sound
            font = Content.Load<SpriteFont>(@"graphics\Font");
            playerWin = Content.Load<SoundEffect>(@"audio\playerWin");
            playerLose = Content.Load<SoundEffect>(@"audio\playerLose");
            paddleStrike = Content.Load<SoundEffect>(@"audio\paddleStrike");

            // add initial game objects
            var yPosition = GameConstants.WindowHeight / 2;
            var xPositionEnemy = GameConstants.WindowWidth - GameConstants.WindowWidth / 10;
            var xPositionPlayer = GameConstants.WindowWidth / 10;
            playerPaddle = new Paddle(Content, @"graphics\Paddle", xPositionPlayer, yPosition);
            enemyPaddle = new Paddle(Content, @"graphics\Paddle", xPositionEnemy, yPosition);

            SpawnBall();

            // set initial health and score strings
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();


            var keyboardState = Keyboard.GetState();
            var gamePadState = GamePad.GetState(PlayerIndex.One);
            if (keyboardState.IsKeyDown(Keys.Up) || keyboardState.IsKeyDown(Keys.W)
                || gamePadState.ThumbSticks.Left.Y > 0.3f)
            {
                playerPaddle.Velocity = -0.4f;
            }
            else if (keyboardState.IsKeyDown(Keys.Down) || keyboardState.IsKeyDown(Keys.S)
                || gamePadState.ThumbSticks.Left.Y < -0.3f)
            {
                playerPaddle.Velocity = 0.4f;
            }
            else
            {
                playerPaddle.Velocity = 0;
            }


            var midPoint = enemyPaddle.CollisionRectangle.Location.Y + enemyPaddle.CollisionRectangle.Height / 2;
            if (midPoint < ball.Middle.Y)
            {
                enemyPaddle.Velocity = 0.4f;
            }
            else if (midPoint > ball.Middle.Y)
            {
                enemyPaddle.Velocity = -0.4f;
            }
            else
            {
                enemyPaddle.Velocity = 0;
            }

            // check for paddle collision
            if (playerPaddle.CollisionRectangle.Intersects(ball.CollisionRectangle)
                || enemyPaddle.CollisionRectangle.Intersects(ball.CollisionRectangle))
            {
                ball.Velocity = new Vector2(-ball.Velocity.X, ball.Velocity.Y);
                paddleStrike.Play();
            }

            // check for collision with sides
            if (ball.CollisionRectangle.Right > GameConstants.WindowWidth)
            {
                // enemy lost
                score[0] = score[0] + 1;
                SpawnBall();
                playerWin.Play();
            }
            else if (ball.CollisionRectangle.Left < 0)
            {
                // player lost
                score[1] = score[1] + 1;
                SpawnBall();
                playerLose.Play();
            }

            enemyPaddle.Update(gameTime);
            playerPaddle.Update(gameTime);
            ball.Update(gameTime);

            scoreString = score[0].ToString() + " - " + score[1].ToString();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();

            ball.Draw(spriteBatch);
            playerPaddle.Draw(spriteBatch);
            enemyPaddle.Draw(spriteBatch);

            spriteBatch.DrawString(font, scoreString, GameConstants.ScoreLocation, Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }

        #region Helper methods

        /// <summary>
        /// Spawns the ball from the middle and a random velocity
        /// </summary>
        private void SpawnBall()
        {
            if (ball == null)
            {
                ball = new Ball(Content, @"graphics\Ball", GameConstants.WindowWidth / 2, GameConstants.WindowHeight / 2);
            }
            else
            {
                ball.Middle = new Point(GameConstants.WindowWidth / 2, GameConstants.WindowHeight / 2);
            }

            float xVelocity = RandomNumberGenerator.NextFloatBetween(0.4f, 0.6f);
            float yVelocity = RandomNumberGenerator.NextFloatBetween(0.4f, 0.6f);
            if (RandomNumberGenerator.Next(10) < 5)
            {
                xVelocity = -xVelocity;
            }

            ball.Velocity = new Vector2(xVelocity, yVelocity);
        }

        #endregion
    }
}
