﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Pong
{
    /// <summary>
    /// All the constants used in the game
    /// </summary>
    public static class GameConstants
    {
        // resolution
        public const int WindowWidth = 800;
        public const int WindowHeight = 600;

        // projectile characteristics
        public const float BallSpeed = 0.3f;

        // burger characteristics
        public const float PaddleSpeed = 0.3f;


        // display support
        const int DisplayOffset = 35;
        public static readonly Vector2 ScoreLocation =
            new Vector2(WindowWidth / 2, DisplayOffset);

    }
}
