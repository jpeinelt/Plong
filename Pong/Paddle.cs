﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pong
{
    class Paddle
    {

        #region Fields

        // graphic and drawing info
        Texture2D sprite;
        Rectangle drawRectangle;
        int halfHeight;

        // movement
        float velocity = 0.0f;
        #endregion

        #region Constructors

        /// <summary>
        ///  Constructs a paddle
        /// </summary>
        /// <param name="contentManager">the content manager for loading content</param>
        /// <param name="spriteName">the sprite name</param>
        /// <param name="x">the x location of the center of the paddle</param>
        /// <param name="y">the y location of the center of the paddle</param>
        public Paddle(ContentManager contentManager, string spriteName, int x, int y)
        {
            LoadContent(contentManager, spriteName, x, y);
            halfHeight = sprite.Height / 2;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the collision rectangle for the paddle
        /// </summary>
        public Rectangle CollisionRectangle
        {
            get { return drawRectangle; }
        }

        /// <summary>
        /// Gets and sets the velocity of the paddle
        /// </summary>
        public float Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Updates the paddle location
        /// </summary>
        /// <param name="gameTime">game time</param>
        public void Update(GameTime gameTime)
        {
            var milliseconds = gameTime.ElapsedGameTime.Milliseconds;
            drawRectangle.Y += (int)(velocity * milliseconds);

            if (drawRectangle.Top < 0)
            {
                drawRectangle.Y = 0;
            }
            else if (drawRectangle.Bottom > GameConstants.WindowHeight)
            {
                drawRectangle.Y = GameConstants.WindowHeight - drawRectangle.Height;
            }
        }

        /// <summary>
        /// Draws the paddle.
        /// </summary>
        /// <param name="spriteBatch">the sprite batch to use</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, drawRectangle, Color.White);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Loads the content for the paddle
        /// </summary>
        /// <param name="contentManager">the content manager to use</param>
        /// <param name="spriteName">the name of the sprite for the burger</param>
        /// <param name="x">the x location of the center of the burger</param>
        /// <param name="y">the y location of the center of the burger</param>
        private void LoadContent(ContentManager contentManager, string spriteName,
            int x, int y)
        {
            // load content and set remainder of draw rectangle
            sprite = contentManager.Load<Texture2D>(spriteName);
            drawRectangle = new Rectangle(x - sprite.Width / 2,
                y - sprite.Height / 2, sprite.Width,
                sprite.Height);
        }

        #endregion
    }
}
